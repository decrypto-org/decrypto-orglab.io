+++
template = "proposals.html"

[[extra.proposals]]
responsible = "Orfeas Stefanos Thyfronitis Litos"
title = "Implementation of Recursive Virtual Channels for Bitcoin"
abstract = """
<p>Bitcoin and other blockchains suffer from severe scalability limitations.
Layer-2 protocols (a.k.a. Payment Channel Networks - PCNs) alleviate this issue
by enabling off-chain payments, where only the involved parties need to
interact, therefore completely bypassing the slow process of mining.
<a href="https://github.com/OrfeasLitos/virtual-payment-channels/raw/master/virtual-channels.pdf">Recursive
Virtual Channels</a> enable opening "virtual" payment channels on top of
existing channels, avoiding the expensive on-chain funding transaction and
facilitating direct payments without implicating intermediaries. This
construction builds on top of the
<a href="https://lightning.network/">Lightning Network</a>, but offers a
different set of tradeoffs on various dimensions (on-chain cost,
network/time/space complexity, privacy, etc.)</p>

<p>Your task will be to build a Proof-of-Concept implementation that showcases
practicality, highlights potential issues and explores which optimisations are
easily attainable.</p>
"""
contact = "orfeas@decrypto.org"
available = true
level = "Master"
location = "Remote"

[[extra.proposals]]
responsible = "Orfeas Stefanos Thyfronitis Litos"
title = "Formal verification of the Lightning Network Security Treatment"
abstract = """
<p>The <a href="https://lightning.network/">Lightning Network</a> promises to
solve the scalability issues of Bitcoin by moving the vast majority of
transactions off-chain, in a "Layer-2" network. In
"<a href="https://raw.githubusercontent.com/OrfeasLitos/PaymentChannels/master/paymentChannels.pdf">A
Composable Security Treatment of the Lightning Network</a>" the specification of
Lightning is implemented in the
<a href="https://eprint.iacr.org/2000/067.pdf">Universal Composition</a>
execution model and its UC security is formally proven. However, the proof
itself is not formally verified as it is written by hand in natural
language.</p>

<p>There exist various formal verification tools (e.g.
<a href="https://coq.inria.fr/">Coq</a>,
<a href="https://www.fstar-lang.org/">F*</a>,
<a href="https://wiki.portal.chalmers.se/agda/pmwiki.php">Agda</a>,
<a href="http://tamarin-prover.github.io/">Tamarin Prover</a>,
<a href="https://prosecco.gforge.inria.fr/personal/bblanche/proverif/">ProVerif</a>,
<a href="https://cryptoverif.inria.fr/">Cryptoverif</a>,
<a href="https://www.easycrypt.info/trac/">EasyCrypt</a>,
<a href="https://eprint.iacr.org/2019/582.pdf">EasyUC</a>,
<a href="https://www.ii.uni.wroc.pl/~tt/papers/CSF-2012.pdf">FCVJP</a>
(<a href="https://www.sec.uni-stuttgart.de/publications/software/simplProtJoana.zip">code</a>),
<a href="http://matita.cs.unibo.it/">matita</a>), some of
which are specifically designed for checking cryptographic implementations, each
with is own set of tradeoffs. Your task will be to develop a formally verified
version of the proof using one (or more) verifier to further improve our
confidence that Lightning is secure.</p>
"""
contact = "orfeas@decrypto.org"
funding = false
available = true
level = "Master"
location = "Remote"
+++
