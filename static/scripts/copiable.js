const MS_IN_DAY = 24 * 60 * 60 * 1000

function dateTimeToDate(dateTime) {
    return new Date([
        dateTime.getMonth(),
        dateTime.getDate(),
        dateTime.getYear()
    ].join('/'))
}

window.addEventListener('DOMContentLoaded', () => {
    $('[data-toggle="tooltip"]').tooltip()

    for (const elem of document.getElementsByClassName('copy-invite')) {
        elem.addEventListener('click', e => {
            const thisCard = elem.closest('.card')
            const invitationElem = thisCard.querySelector('.invite-text')
            const seminarDateTime = new Date(thisCard.querySelector('time').dataset['datetime'])
            const seminarDate = dateTimeToDate(seminarDateTime)
            const nowDateTime = new Date()
            const nowDate = dateTimeToDate(nowDateTime)
            const dateDiff = seminarDate - nowDate
            let diffStr = 'On'

            if (dateDiff < MS_IN_DAY) {
                diffStr = 'Today,'
            }
            else if (dateDiff < 2 * MS_IN_DAY) {
                diffStr = 'Tomorrow,'
            }
            invitationElem.querySelector('.date-diff').innerText = diffStr
            let affiliation = invitationElem.querySelector('.affiliation').innerText

            if (affiliation.indexOf('of') > -1) {
                // heuristic for the use of "the" in front of affiliation
                invitationElem.querySelector('.affiliation-article').innerText = 'the'
            }

            let invitationText = invitationElem.innerText
            invitationText = invitationText
                            .split('\n').map(x => x.trim()).join('\n')
                            .replace(/([^\n])\n(?!\n)/g, '$1 ')
                            .replace(/(^|[^\n])\n{2,}(?!\n)/g, "$1\n\n")
                            .trim()

            navigator.clipboard.writeText(invitationText)

            setTimeout(() => {
                $(thisCard).find('[data-trigger]').tooltip('hide')
            }, 2000)

            e.preventDefault()
        })
    }
})
